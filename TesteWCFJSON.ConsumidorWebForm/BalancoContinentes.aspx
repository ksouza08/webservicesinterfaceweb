﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BalancoContinentes.aspx.cs" Inherits="TesteWCFJSON.ConsumidorWebForm.BalancoContinentes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent"
    runat="server">
    <script type="text/javascript"
        src="http://www.google.com/jsapi"></script>
    <script type="text/javascript">
        function gerarVisualizacao(tipo, idControle) {
            var ano = '<%:Request.QueryString["ano"]%>';
            
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Continente');
            data.addColumn('number', 'Valor');

            var urlBalanco = 'http://localhost:50143/' + 'BalancoComercialWS.svc/continentes/' + ano;

           

            var retornoWS;
            $.ajax(
                {
                    type: 'GET',
                    url: urlBalanco,
                    dataType: 'json',
                    crossDomain: true,
                    async: false,
                    success: function (data) {
                        retornoWS = data;
                    },
                    error: function (a, b, c) {
                        alert("Error Get" + a);
                    }
                });

            var dadosBalanco = [];
            $.each(retornoWS, function (i, balanco) {
                if (tipo == 'EXPORTAÇÕES') {
                    dadosBalanco.push([
                        balanco.Continente.toString(),
                        parseFloat(balanco.ValorExportado)]);
                } else {
                    dadosBalanco.push([
                        balanco.Continente.toString(),
                        parseFloat(balanco.ValorImportado)]);
                }
            });
            data.addRows(dadosBalanco);

            var formatter = new google.visualization.NumberFormat({
                prefix: 'R$ (milhões)',
                decimalSymbol: ',',
                groupingSymbol: '.',
                fractionDigits: 1
            });
            formatter.format(data, 1);

            var titulo;
            if (titulo == 'EXPORTACOES')
                titulo = 'Exportações por Continente - ' + ano;
            else
                titulo = 'Importações por Continente - ' + ano;

            var options = {
                title: titulo,
                is3D: true,
                height: 300,
                width: 450
            };

            var controle = new google.visualization.PieChart(
                document.getElementById(idControle));
            controle.draw(data, options);
        }
    </script>

    <script type="text/javascript">
        jQuery.support.cors = true;

        google.load("visualization", "1",
            { packages: ["corechart"] });

        
        google.setOnLoadCallback(gerarVisualizacaoGraficoExportacoes);
        
        function gerarVisualizacaoGraficoExportacoes() {
            gerarVisualizacao('EXPORTACOES', 'divExportacoesContinentes');
        }

        google.setOnLoadCallback(gerarVisualizacaoGraficoImportacoes);

        function gerarVisualizacaoGraficoImportacoes() {
            gerarVisualizacao('IMPORTACOES', 'divImportacoesContinentes');
        }

        

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent2"
    runat="server">
    <h4>Balanço por Continente - 
        <%: Request.QueryString["ano"] %>
    </h4>
    <br />
    <div>
        <div id="divExportacoesContinentes"
            style="float: left;">
        </div>
        <div id="divImportacoesContinentes"
            style="margin-left: 450px">
        </div>
    </div>
    <br />
    <asp:HyperLink ID="lnkVoltar"
        NavigateURL="~/Default.aspx"
        runat="server">Voltar</asp:HyperLink>
</asp:Content>