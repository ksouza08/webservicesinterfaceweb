﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// As Informações Gerais sobre um assembly são controladas por meio do 
// conjunto de atributos a seguir. Altere esses valores de atributo para modificar as informações
// associadas a um assembly.
[assembly: AssemblyTitle("TesteWCFJSON.ConsumidorWebForm")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("TesteWCFJSON.ConsumidorWebForm")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Configurar o ComVisible como false torna os tipos desse assembly invisíveis 
// para componentes COM.  Se for necessário acessar um tipo nesse assembly a partir do 
// COM, defina o atributo ComVisible como true nesse tipo.
[assembly: ComVisible(false)]

// A GUID a seguir será referente à ID do typelib se este projeto for exposto ao COM
[assembly: Guid("21c91751-7035-4409-b89f-c726373edf9f")]

// As informações de versão de um assembly consistem nos quatro valores a seguir:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// É possível especificar todos os valores ou utilizar como padrão os Números de Revisão e da Versão 
// usando o '*' como mostrado abaixo:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
