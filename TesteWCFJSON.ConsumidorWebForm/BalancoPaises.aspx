﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BalancoPaises.aspx.cs" Inherits="TesteWCFJSON.ConsumidorWebForm.BalancoPaises" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent"
    runat="server">
    <script type="text/javascript"
        src="http://www.google.com/jsapi"></script>
    <script type="text/javascript">
        function gerarVisualizacao(tipo, idControle, options) {

            var ano = '<%: Request.QueryString["ano"]%>';
                        
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'País');
            data.addColumn('number', 'Exportações (em milhões R$)');
            data.addColumn('number', 'Importações (em milhões R$)');

            var urlBalanco =
              'http://localhost:50143/' +
             'BalancoComercialWS.svc/paises/' + ano;

            

            var retornoWS;
            $.ajax(
                {
                    type: 'GET',
                    url: urlBalanco,
                    dataType: 'json',
                    crossDomain: true,
                    async: false,
                    success: function (data) {
                        retornoWS = data;                        
                    },
                    error: function (a, b, c) {
                        alert("Erro Get");
                    }                   

                });

            

            var dadosBalanco = [];
            $.each(retornoWS, function (i, balanco) {
                if (tipo == 'IntensityMap') {
                    dadosBalanco.push([
                        balanco.Sigla.toString(),
                        parseFloat(balanco.ValorExportado),
                        parseFloat(balanco.ValorImportado)]);
                }
                else {
                    dadosBalanco.push([
                        balanco.Pais.toString(),
                        parseFloat(balanco.ValorExportado),
                        parseFloat(balanco.ValorImportado)]);
                }
            });
            data.addRows(dadosBalanco);

            var formatter = new google.visualization.NumberFormat(
                {
                    decimalSymbol: ',',
                    groupingSymbol: '.',
                    fractionDigits: 1
                });
            formatter.format(data, 1);
            formatter.format(data, 2);

            var controle;
            if (tipo == 'IntensityMap') {
                controle = new google.visualization.IntensityMap(
                    document.getElementById(idControle));
            }
            else if (tipo == 'Table') {
                controle = new google.visualization.Table(
                    document.getElementById(idControle));
            }
            controle.draw(data, options);
        }
    </script>

    <script type="text/javascript">
        jQuery.support.cors = true;

        google.load("visualization", 1,
            { packages: ["intensitymap"] });
        google.setOnLoadCallback(gerarVisualizacaoMapaIntensidade);

        function gerarVisualizacaoMapaIntensidade() {
            gerarVisualizacao('IntensityMap', 'divMapaPaises', {});
        }

        google.load('visualization', '1', { packages: ['table'] });
        google.setOnLoadCallback(gerarVisualizacaoTabelaPaises);

        function gerarVisualizacaoTabelaPaises() {
            gerarVisualizacao('Table', 'divTabelaPaises',
                {
                    width: 600
                });
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent2" runat="server">
    <h4>Balanço por País - <%: Request.QueryString["ano"] %></h4>
    <br />
    <div id="divMapaPaises">
    </div>
    <br /><br />
    <div id="divTabelaPaises">
    </div>
    <br />
    <asp:HyperLink ID="lnkVoltar"
        NavigateUrl="~/Default.aspx"
        runat="server">Voltar</asp:HyperLink>
</asp:Content>
